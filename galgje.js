$(function () {
  $("#us_btn").click(function(){
    $("#us_cont").toggle('slide', {direction: 'up'});;
  });
  $("#ch_btn").click(function(){
    $("#characterform").toggle('slide', {direction: 'up'});;
  });
  $(".slideright-btn").click(function(){
    $(".slideright-cont").toggle('slide', {direction: 'left'});
    $("#caret_slct_r").toggleClass('fa-caret-right fa-caret-left')
  });
  $(".slideleft-btn").click(function(){
    $(".slideleft-cont").toggle('slide', {direction: 'right'});
    $("#caret_slct_l").toggleClass('fa-caret-left fa-caret-right')
  });
  function fetchUnderscores() {
    $.ajax("player.php?ajax=true")
      .done(function (data) {
          let counter = 0;
          $('#word').html('');
          $('#chances').html('');
          $('#galgjeimg').html('');
          let underscores = data[1];
          let word = data[0];
          let chances = data[2];
          let galgjeimg = data[4];
          for (let i=0;i<underscores.length;i++) {
            if (underscores[i] === "_") {
              counter += 1;
              $('#word').append(`<li>${underscores[i]}</li>`);
            } else{
              $('#word').append(`<li>${underscores[i]}</li>`);
            }

          }
          $('#chances').append(`<p>You still have ${chances} chances.</p>`);
          $('#galgjeimg').append(`<img id="galgje_img" src="img/${galgjeimg}.jpg">`);
          if (counter === word.length) {
            $('#characters').html('');
          }

          if (counter === 0) {
            $('#congratulations').html('');
            $('#congratulations').append(`<p class="congrats">Congratulations you win!</p>`);
            $('#image-cont').html('');
            $('#image-cont').append(`<img class="image-cent" src="img/win.gif" alt="win image" title="win"/>`);
            $("#modal-pop").css("display", "block");
          } else {
            $('#congratulations').html('');
            $('#image-cont').html('');
            $("#modal-pop").css("display", "none");
          }

          if (chances === 0) {
            $('#congratulations').html('');
            $('#congratulations').append(`<p class="congrats">You have lost! Better luck next time..</p>`);
            $('#image-cont').html('');
            $('#image-cont').append(`<img class="image-cent" src="img/lose.gif" alt="lose image" title="lose"/>`);
            $("#modal-pop").css("display", "block");
          }

    });
  }

  function showCharacters() {
    $.ajax("player.php?ajax=true")
      .done(function(data) {
        $('#chances').html('');
        $('#galgjeimg').html('');
        let new_character = data[5];
        let chances = data[2];
        let galgjeimg = data[4];
        $('#chances').append(`<p>You still have ${chances} chances.</p>`);
        $('#characters').append(`<li>${new_character}</li>`);
        $('#galgjeimg').append(`<img id="galgje_img" src="img/${galgjeimg}.jpg">`);
      });
  }

  $('#guessbutton').on('click', function () {
    let character = $('#character').val();
      if (character.length < 1 || character.length > 1) {
        window.alert("Your entry is not valid");
      } else {
        $.ajax({method: "POST", url: "character.php", data: {character: character}})
          .done(function (data) {
            fetchUnderscores();
            showCharacters();
          })
        }
  });

  fetchUnderscores();
  setInterval(fetchUnderscores, 500);
});