<?php
    #when character is retrieved from POST
    #if (isset($_POST['title']) && isset($_POST['content'])) {
    $input = file_get_contents("word.json");
    $data = json_decode($input);
    $character = htmlspecialchars($_POST['character']); #placeholder
    $wordsplit = str_split($data[0]);
    $wordlength = strlen($data[0]);
    $chances = htmlspecialchars($data[2]);
    $galgjeindex = htmlspecialchars($data[3]);
    $correct = false;
    $galgjefiles = array("galgje1", "galgje2", "galgje3", "galgje4", "galgje5", "galgje6", "galgje7", "galgje8", "galgje9", "galgje10", "galgje11", "galgje12");
    for ($i = 0; $i < $wordlength; $i++) { #loop through word characters
        if ($character == $wordsplit[$i]) {
            $data[1][$i] = $character; #replace the underscore with the character
            $correct = true;
        }
    }
    if ($correct == false) { #return false when the character is wrong
        $chances -= 1;
        $galgjeindex += 1;
    }
    $data[2] = $chances;
    $data[3] = $galgjeindex;
    $data[4] = $galgjefiles[$galgjeindex];
    $data[5] = $character;
    $output = json_encode($data);
    file_put_contents("word.json", $output);

?>
