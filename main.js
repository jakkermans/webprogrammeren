$(function() {
	$(".slideleft-btn").click(function(){
		/*if ($("#crt-check")hasClass("fa-caret-left")) {
			var options = "";
		}*/
		$(".slideleft-cont").toggle('slide', {direction: 'right'});
		$(".caret-icon").toggleClass('fa-caret-left fa-caret-right')
	});
	$("#host_btn").click(function(){
		$(".host_slide").toggle('slide', {direction: 'up'});
	});
	$("#submit").click(function(){
		$("#word_form").validate({
			rules: {
				host: {
					required: true,
					minlength: 4,
					nowhitespace: true,
					lettersonly: true
				}
			},
			messages: {
				host: {
					required: 'Please enter a word',
					minlength: 'Your word needs to have at least 4 letters',
					nowhitepace: 'No spaces allowed',
					lettersonly: 'Only letters allowed'
				}
			},
			submitHandler: function(form) {
				$.ajax({type: "POST", url: "word.php", data: {word: $('#host').val()}})
				$("#host").val('');
				$( '#succes_message' ).text('Your word has succesfully saved');
				setTimeout(function() {
					$( '#succes_message' ).text('');
				}, 3000);
			}
            
		});
    setTimeout(function() {
        window.location.replace("host.php");
        }, 2000);
    
	});
    $("#playerbutton").click(function(){
        window.location.replace("player.php");
        });
});

