<?PHP
 #when word.php is finished, and word is retrieved from POST
 #if (isset($_POST['title']) && isset($_POST['content'])) { 
    $input = file_get_contents("word.json");
    $data = json_decode($input);
    $newword = $data['word']; #placeholder
    unset($data); #clears old word from the json
   // $wordarray[] = ($newword);
   // $wordlength = strlen($wordarray[0]);
    $wordlength = strlen($newword);
    $underscores = "";
    for ($i = 0; $i < $wordlength; $i++) { #creates underscores based on word length
        $underscores .= "_";
    }
  
    //$wordarray[] = ($underscores);
    $data[] = array("word" => $newword, "underscores" => $underscores);
    $output = json_encode($data);
    file_put_contents("word.json", $output);  
?>
