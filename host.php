<?php
if (isset($_GET['ajax'])) {
    $data = file_get_contents("word.json");
    header('Content-Type: application/json');
    echo $data;
    die();
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Play galgje!</title>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' 
    type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="host.css">
</head>
<body>
	<div class="container">
		<div class="underscore_sect">
			<div class="action-button shadow animate red" id="us_btn" name="us_btn"></div>
			<div id="us_cont">
                <ul id="word"></ul>
            </div>
		</div>
		<div id="slide_menus">
            <button class="slide-btn slideright-btn">
                <i class="fa fa-caret-left caret-icon" id="caret_slct_r"></i>
            </button>
            <div class="slideright-cont">
                <div id="chances"></div>
            </div>
        </div>
        <div id="modal-pop">
            <div class="modal-cont">
                <div id="congratulations"></div>
                <div id="image-cont"></div>
                <div id="playagain"><input type="submit" class="animate" id="modal_btn" value="play again" onclick="document.location.href='index.php';"/></div>
            </div>
        </div>
		<div id="galgjeimg"></div>
	</div>
<script src="js/jquery-ui-1.12.1.custom/external/jquery/jquery.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.12.1.custom/jquery-ui.min.js" type="text/javascript"></script><!--loading custom jqueryUI for slide effect -->
<script src="host.js"></script>
</body>
</html>