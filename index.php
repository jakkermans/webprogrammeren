<?PHP
if (isset($_GET['ajax'])) {
	$data = file_get_contents("word.json"); /* Leest books.json in als string */
	header('Content-Type: application/json'); /* Stel de header in */
	echo $data;
	die();
}
?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Cache-control" content="no-cache">
    <link rel="stylesheet" href="main.css?v=<?php echo time(); ?>" type="text/css" /><!-- clear cache so css loads porperly in XXAMP-->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans" />
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' 
    type='text/css'>
    <link href="js/jquery-ui-1.12.1.custom/jquery-ui.structure.css" rel='stylesheet'><!--loading structure of jquery UI for functionality-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>game</title>
  </head>
  <body>
	<div id="jq_lnk_mn">
    	<button class="slideleft-btn">
    		<i class="fa fa-caret-right caret-icon"></i>
    	</button>
    	<div class="slideleft-cont">
    		<h2>Welcome to our Game!</h2>
    		<p>Click host to Enter a Word</p>
        <p>Only the player should click the Player button</p>
    	</div>
	</div>
	<div class="container">
		<div class="btn_container">
			<div class="host_section">
  				<div class="action-button shadow animate red" id="host_btn" name="host">Host</div>
  				<form class="host_slide" id="word_form">
  					<label class="form-lbl" for="host">Enter A Word</label>
				  	<input type="text" id="host" name="host"/>
            <div id="succes_message"></div>
				  	<input type="submit" class="animate" id="submit" value="Select Word"/>          
  				</form>
  			</div>
  			<div class="host_section">
  				<div id="playerbutton" class="action-button shadow animate red">Player</div>
  			</div>
		</div>
	</div>
	<script src="js/jquery-ui-1.12.1.custom/external/jquery/jquery.js" type="text/javascript"></script>
	<script src="js/jquery-ui-1.12.1.custom/jquery-ui.min.js" type="text/javascript"></script><!--loading custom jqueryUI for slide effect -->
	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.17.0/jquery.validate.min.js"></script><!-- Form validation-->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script><!-- Additional validator rules-->
	<script src="main.js"></script><!-- loading javascript -->
  </body>
</html>
