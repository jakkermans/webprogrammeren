$(function () {
  $("#us_btn").click(function(){
       $("#us_cont").toggle('slide', {direction: 'up'});;
  });
  $(".slideright-btn").click(function(){
    $(".slideright-cont").toggle('slide', {direction: 'left'});
    $("#caret_slct_r").toggleClass('fa-caret-right fa-caret-left')
  });
   function inSync() {
       $.ajax("host.php?ajax=true")
           .done(function(data) {
               $('#us_btn').html('');
              let playword = data[0];
              $('#us_btn').append(`The word for this game is ${playword}`);
              console.log("check");
               let counter = 0;
               $('#word').html('');
               $('#chances').html('');
               $('#galgjeimg').html('');
               let underscores = data[1];
               let word = data[0];
               let chances = data[2];
               let galgjeimg = data[4];
               let new_character = data[5];
               for (let i=0;i<underscores.length;i++) {
                   if (underscores[i] === "_") {
                       counter += 1;
                       $('#word').append(`<li>${underscores[i]}</li>`);
                   } else{
                       $('#word').append(`<li>${underscores[i]}</li>`);
                   }

               }
               $('#chances').append(`<p>The player has ${chances} chances left.</p>`);
               $('#galgjeimg').append(`<img id="galgje_img" src="img/${galgjeimg}.jpg">`);
               if (counter === word.length) {
                   $('#characters').html('');
               }

               if (counter === 0) {
                   $('#congratulations').html('');
                   $('#image-cont').html('');
                   $('#congratulations').append(`<p class="congrats">The player has won the game</p>`);
                   $('#image-cont').append(`<img class="image-cent" src="img/win.gif" alt="win image" title="win"/>`);
                   $("#modal-pop").css("display", "block");
               } else {
                   $('#congratulations').html('');
                   $('#image-cont').html('');
                   $("#modal-pop").css("display", "none");
               }

               if (chances === 0) {
                   $('#congratulations').html('');
                   $('#image-cont').html('');
                   $('#congratulations').append(`<p class="congrats">The player has lost the game</p>`);
                   $('#image-cont').append(`<img class="image-cent" src="img/lose.gif" alt="lose image" title="lose"/>`);
                   $("#modal-pop").css("display", "block");
               }
           });
   }

   inSync();
   setInterval(inSync, 1000);
});