# what still needs to be done:
- a host.php that displays the game status for the host. Needs to be in sync with the player.php
- function in the javascript that keeps track of incorrect guesses
- some sort of overview for all characters that the player has guessed (so he knows which ones he has entered previously)
- galg visualisation (idea was through a series of images)
- css so it doesnt look like shit

# optional ideas:
- some sort of system that verifies if the entered word is actually an existing word
- a field for the player to enter the entire word at once if he thinks he knows it, and scripts for this

# currently implemented:

- (addword.php) script that takes the word from the host and creates the data for it in the json
- (character.php) script that takes a character/letter from the player and checks if the word contains it
  if yes, it updates the underscores in the data. Also returns true/false for keeping track of incorrect characters
- (player.php) page for the player that displays the "hidden" word through underscores
- (galgje.js) script that retrieves the json word data and displays it on the player.php
- (galgje.css) css file for the player.php
- Message implemented in galgje.js that when the players guessed all the correct letters, it says 'Congratulations'. Could be altered.
- Function in galgje.js that when the player guesses a correct letter, it displays the correct letters.
